## whathammers

**whathammers** is a command line tool for quick analysis of Apache log files.

Features:

- Configurable log file format, using the same syntax as the [Apache LogFormat directive](https://httpd.apache.org/docs/2.2/mod/mod_log_config.html#formats);
- Configurable reports, with sensible defaults;
- Does not require a web interface, does not need to be pre-installed, etc.

Anti-features:

- Implemented in Python, so if you can't install Python on your server, you'll need to copy the log files back to some other machine;
- Parsing is done in-memory, so may not be appropriate for very large log files.

See the [example output](#example-output)

### Usage

```
usage: whathammers [-h] [-f FORMAT] [-r REPORTS] [-c COUNT]
                   [logfile [logfile ...]]
```

Where:
- `logfile` is the path to one or more Apache httpd log file. If none is present, **whathammers** will read from stdin;
- `-h, --help` displays the help message and exits;
- `-f FORMAT, --format FORMAT` defines the format of the Apache httpd logfile, using the [Apache logfile format](https://httpd.apache.org/docs/2.2/mod/mod_log_config.html#formats). Default is the Apache Common Log Format, or `%h %l %u %t "%r" %>s %b`. You should use the format defined for your server/vhost under the LogFormat directive;
- `-r REPORTS, --reports REPORTS` defines the reports to generate. This is a comma separated list of `<fieldname>:[h|t|b]`. For discrete fields (eg. `remote_host`, `request_url_path`, etc.) adding `h` will show the top N values of `<fieldname>` by number of hits; `t` by total time and `b` by time per hit. For continuous fields (only `time_received` is considered continuous) this will divide the available range in N slices and show the number of hits, total time or time per hit for each slice. The list of available fields is output at top of the report. Also note that `t` and `b` reports are only available if request times are logged. Default is `remote_host:ht,request_header_user_agent:ht,request_url_path:hb,time_received:hb`;
- `-c COUNT, --count COUNT` defines  number of items to return eg. top 5 IP addresses. Default 5.

### Examples

Here are a few invokation examples. You can view an [example output](#example-output) below.

Analyse a log file with the default reports, and the Apache Common Log format:

```shell
whathammers access_log
```

Analyse a log file with a custom log format. The format is copied directly from the Apache configuration:

```shell
whathammers --format '%h %l %u %t "%r" %>s %b "%{Referer}i" "%{User-Agent}i" time=%D host=%v keepalive=%X forwarded_for="%{X-Forwarded-For}i"' access_log
```

Analyze a log file, and output custom reports (here we're interested in stats depending on the query string):

```shell
whathammers --reports request_url_query:ht
```

### Instalation

**whathammers** is available as a PyPI package, so you can install it with:

```shell
pip install whathammers
```

If you're not a Pythonista:

First you need to have Python and pip installed. Most likely these are already installed if you're runing a Linux system. How to install these will depend on your distribution. For Debian derivatives, you would do:

```shell
sudo apt-get install python python-pip
```

In Python you can install packages in isolation, using virtual environments. I would recomend doing this so you don't install packages in your global namespace. To do this you would need an additional package. In Debian derivatives:

```shell
sudo apt-get install python-virtualenv
```

This being done, you can create a virtual environment, for example in `/usr/local/whathammers`:

```shell
sudo mkdir /usr/local/whathammers
sudo virtualenv /usr/local/whathammers/venv
```

Now install **whathammers** in the virtual environment:

```shell
sudo /usr/local/whathammers/venv/bin/pip install whathammers
```

And finally add **whathammers** to your path:

```shell
sudo ln -s /usr/local/whathammers/venv/bin/whathammers /usr/local/bin/whathammers
``` 

To delete **whathammers** all you need to do is delete the folder `/usr/local/whathammers`.

### <a name="example-output"></a> Example output

This an example output, using the default reports. Note that IP addresses and page urls have been scrambled.

```
Parsing files....
Logged fields : conn_status, remote_host, remote_logname, remote_user, request_first_line, request_header_referer, request_header_user_a
gent, request_header_user_agent__browser__family, request_header_user_agent__browser__version_string, request_header_user_agent__is_mobi
le, request_header_user_agent__os__family, request_header_user_agent__os__version_string, request_header_x_forwarded_for, request_http_v
er, request_method, request_url, request_url_fragment, request_url_hostname, request_url_netloc, request_url_password, request_url_path,
 request_url_port, request_url_query, request_url_query_dict, request_url_query_list, request_url_query_simple_dict, request_url_scheme,
 request_url_username, response_bytes_clf, server_name, status, time_received, time_received_datetimeobj, time_received_isoformat, time_
received_tz_datetimeobj, time_received_tz_isoformat, time_received_utc_datetimeobj, time_received_utc_isoformat, time_us

Top 5 remote host by hits
-------------------------
           33.33.33.33: 18496 hits (17.04% of total)
        33.333.333.333: 5090 hits (4.69% of total)
     bla.somewhere.com: 3920 hits (3.61% of total)
         333.333.33.33: 3436 hits (3.17% of total)
and.somewhere.else.com: 2567 hits (2.36% of total)

Top 5 remote host by total time
-------------------------------
           33.33.33.33: 30575.46 seconds (21.88% of total)
     bla.somewhere.com: 4010.18 seconds (2.87% of total)
        33.333.333.333: 3932.08 seconds (2.81% of total)
         333.333.33.33: 3880.73 seconds (2.78% of total)
and.somewhere.else.com: 3581.93 seconds (2.56% of total)

Top 5 user agent by hits
------------------------
Mozilla/5.0 (compatible; MJ12bot/v1.4...: 26365 hits (24.29% of total)
Mozilla/4.0 (compatible; MSIE 9.0; Wi...: 11216 hits (10.33% of total)
Mozilla/5.0 (Windows NT 6.3; Win64; x...: 9522 hits (8.77% of total)
Mozilla/5.0 (Macintosh; Intel Mac OS ...: 8154 hits (7.51% of total)
Mozilla/5.0 (compatible; Googlebot/2....: 4596 hits (4.23% of total)

Top 5 user agent by total time
------------------------------
Mozilla/5.0 (compatible; MJ12bot/v1.4...: 38073.62 seconds (27.24% of total)
Mozilla/5.0 (Macintosh; Intel Mac OS ...: 18340.86 seconds (13.12% of total)
Mozilla/4.0 (compatible; MSIE 9.0; Wi...: 11810.71 seconds (8.45% of total)
Mozilla/5.0 (Windows NT 6.3; Win64; x...: 11578.16 seconds (8.28% of total)
Mozilla/5.0 (compatible; Googlebot/2....: 7003.51 seconds (5.01% of total)

Top 5 request_url_path by hits
------------------------------
                /a/path: 14397 hits (13.26% of total)
/another/path/or/sorts/: 6580 hits (6.06% of total)
                      /: 5942 hits (5.47% of total)
     /paths/everywhere/: 4133 hits (3.81% of total)
       /the/super/path/: 3597 hits (3.31% of total)

time_received by hits, 5 slices
-------------------------------
2017-04-23 04:14:29: ---------------- (12483 hits)
2017-04-23 21:13:08: ------------------------- (18810 hits)
2017-04-24 14:11:47: -------------------------- (19702 hits)
2017-04-25 07:10:26: ------------------------------------- (27745 hits)
2017-04-26 00:09:05: ---------------------------------------- (29814 hits)

time_received by time per hit, 5 slices
---------------------------------------
2017-04-23 04:14:29: --------------------------------- (1.43 seconds)
2017-04-23 21:13:08: ------------------------------- (1.30 seconds)
2017-04-24 14:11:47: ---------------------- (955.42 miliseconds)
2017-04-25 07:10:26: ------------------------ (1.03 seconds)
2017-04-26 00:09:05: ---------------------------------------- (1.68 seconds)
```

### Copyright

This package is © 2017 Alice Heaton, released under the terms of the GNU GPL v3 (or at your option a later version).
